package unicamp.ic.inf335.friends;

import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.bson.Document;

import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import com.mongodb.BasicDBObject;
import com.mongodb.MongoException;
import com.mongodb.client.MongoClient;

public class Friends {
	private static final int FIRST_SEASON = 1;
	private static final int LAST_SEASON = 10;

	public static void main(String[] args) {
		try {
			Logger mongoLogger = Logger.getLogger("org.mongodb.driver");
			mongoLogger.setLevel(Level.SEVERE);
			MongoClient mongoClient = MongoClients.create();
			MongoDatabase database = mongoClient.getDatabase("friends");
			MongoCollection<Document> collection = database.getCollection("friends");

			Scanner sc = new Scanner(System.in);
			int season = 0;

			while (true) {
				System.out.println("Entre com um numero de temporada de 1 a 10 (-1 para sair): ");
				season = checkNumberRange(sc);
				if (season < 0) {
					System.out.println("FIM!!!!!");
					break;
				} else if (season > 0) {
					BasicDBObject whereQuery = new BasicDBObject();
					whereQuery.put("season", season);
					MongoCursor<Document> cursor3 = collection.find(whereQuery).iterator();
					System.out.println("Episodios da temporada " + season + ":");
					while (cursor3.hasNext()) {
						Document cursor = cursor3.next();
						System.out.println("Nome do episodio: " + cursor.get("name"));
					}
				}
				System.out.println("\n");
			}
		} catch (MongoException e) {
			e.printStackTrace();
		}
	}

	private static int checkNumberRange(Scanner in) {
		if (in.hasNextInt()) {
			int season = in.nextInt();
			if (season >= FIRST_SEASON && season <= LAST_SEASON) {
				return season;
			} else if (season == -1) {
				return -1;
			}
		} else {
			in.next();
		}
		return 0;
	}

}
